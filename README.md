morse_wav
=============

This project encodes arbitrary text as morse-encoded WAV files
with configurable frequency, speed, and sample rates.

Also included is a second script showing how the first can be
used as a support library

Usage
--------------

```text
usage: Generates a WAV file containing a morse-code encoding of a provided string
       [-h] [-o,--output output.wav] [-S,--space] [-P,--punctuation]
       [-d,--dit DIT_MS] [-D,--dah DAH_MS] [-s,--sep SEP_MS]
       [-c,--char-sep CHAR_SEP_MS] [-v,--volume VOL] [-f,--freq FREQ]
       [-b,--bitrate SAMPLE_RATE]
       TEXT

positional arguments:
  TEXT                  The text to encode

optional arguments:
  -h, --help            show this help message and exit
  -o,--output output.wav
                        The path to the output WAV file
  -S,--space            Encode spaces in morse; otherwise spaces are stripped
  -P,--punctuation      Encode punctuation in morse; otherwise punctuation is
                        stripped
  -d,--dit DIT_MS       The duration in ms of a . (ms)
  -D,--dah DAH_MS       The duration in ms of a - (ms)
  -s,--sep SEP_MS       The duration of the silence between consecutive beeps
                        in the same letter (ms)
  -c,--char-sep CHAR_SEP_MS
                        The duration the silence between characters (ms)
  -v,--volume VOL       The volume of the output file (0-1)
  -f,--freq FREQ        The frequency of the output tones (Hz)
  -b,--bitrate SAMPLE_RATE
                        The output bitrate rate
```
