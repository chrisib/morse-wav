#!/usr/bin/python
"""
morse_wav
================

This file can either be used as a stand-alone executable script, or as a support library
for other scripts.  The WAV encoding is based on www.daniweb.com/code/snippet263775.html and
https://stackoverflow.com/questions/33879523/python-how-can-i-generate-a-wav-file-with-beeps

@author     Chris Iverach-Brereton <ve4cib@gmail.com>
@summary    A library for encoding text in morse code WAV files
"""
import math
import wave
import struct
import sys
import argparse

# the core morse code alphabet & numbers
MORSE_CODE = {
    'a':'.-',
    'b':'-...',
    'c':'-.-.',
    'd':'-..',
    'e':'.',
    'f':'..-.',
    'g':'--.',
    'h':'....',
    'i':'..',
    'j':'.---',
    'k':'-.-',
    'l':'.-..',
    'm':'--',
    'n':'-.',
    'o':'---',
    'p':'.--.',
    'q':'--.-',
    'r':'.-.',
    's':'...',
    't':'-',
    'u':'..-',
    'v':'...-',
    'w':'.--',
    'x':'-..-',
    'y':'-.--',
    'z':'--..',

    '0':'-----',
    '1':'.----',
    '2':'..---',
    '3':'...--',
    '4':'....-',
    '5':'.....',
    '6':'-....',
    '7':'--...',
    '8':'---..',
    '9':'----.',
}

# extended morse code punctuation
MORSE_CODE_PUNCTUATION = {
    '.':'.-.-.-',
    ',':'--..--',
    '?':'..--..',
    '\'':'.---.',
    '!':'-.-.--',
    '/':'-..-.',
    '(':'-.--.',
    ')':'-.--.-',
    '&':'.-...',
    ':':'--..--',
    ';':'-.-.-.',
    '=':'-...-',
    '+':'.-.-.',
    '-':'-....-',
    '_':'..--.-',
    '"':'.-..-.',
    '$':'...-..-',
    '@':'.--.-.'
}

# the space characer encoded in morse
MORSE_CODE_SPACE = '..--'

# default duration for a . character in morse
DIT_DURATION_MS = 75

# default duration for a - character in morse
DAH_DURATION_MS = 150

# the period of silence between . or - characters within the same letter
DITDAH_SEPARATION_MS = 50

# the period of silence between letters
LETTER_SEPARATION_MS = 100

# 44100 - CD-quality
#  8000 - low-quality
SAMPLE_RATE = 44100.0

# the output tone frequency
FREQUENCY_HZ = 880.0

# the volume multiplier
# should be in the range 0.0 (mute) to 1.0 (full volume)
# note that max volume can result in some clipping & artefacts,
# which is why we default to 0.75
VOLUME = 0.75

def add_beep(audio, duration_ms, volume=VOLUME, frequency=FREQUENCY_HZ, sample_rate=SAMPLE_RATE):
    """
    Add a single beep to the end of the audio array
    """
    sample_rate=float(sample_rate)

    num_samples = duration_ms * (sample_rate / 1000.0)

    for x in range(int(num_samples)):
        audio.append(volume * math.sin(2 * math.pi * frequency * ( x / sample_rate )))


def add_silence(audio, duration_ms, sample_rate=SAMPLE_RATE):
    """
    Add a period of silence to the end of the audio array
    """
    sample_rate=float(sample_rate)

    num_samples = duration_ms * (sample_rate / 1000.0)

    for x in range(int(num_samples)):
        audio.append(0.0)

    return

def save_wav(path, audio, sample_rate=SAMPLE_RATE):
    """
    Save the waveform recorded in the audio array to the specified path
    """
    sample_rate=float(sample_rate)

    # Open up a wav file
    wav_file=wave.open(path,"w")

    # wav params
    nchannels = 1

    sampwidth = 2

    # 44100 is the industry standard sample rate - CD quality.  If you need to
    # save on file size you can adjust it downwards. The stanard for low quality
    # is 8000 or 8kHz.
    nframes = len(audio)
    comptype = "NONE"
    compname = "not compressed"
    wav_file.setparams((nchannels, sampwidth, sample_rate, nframes, comptype, compname))

    # WAV files here are using short, 16 bit, signed integers for the
    # sample size.  So we multiply the floating point data we have by 32767, the
    # maximum value for a short integer.  NOTE: It is theortically possible to
    # use the floating point -1.0 to 1.0 data directly in a WAV file but not
    # obvious how to do that using the wave module in python.
    for sample in audio:
        wav_file.writeframes(struct.pack('h', int( sample * 32767.0 )))

    wav_file.close()

def encode_string(audio, to_encode, encode_punctuation=False, encode_spaces=False,
    dit=DIT_DURATION_MS, dah=DAH_DURATION_MS, sep=DITDAH_SEPARATION_MS, char_sep=LETTER_SEPARATION_MS,
    volume=VOLUME, frequency=FREQUENCY_HZ, sample_rate=SAMPLE_RATE):
    """
    Encodes a string as morse code, adding the audio to the waveform array, returning a string representation of the encoded data.

    If encode_spaces is true the space and tab characters are encoded with ..--.  Otherwise all whitespace is stripped

    If encode_punctuation is true, all supported punctuation is encoded according to MORSE_CODE_PUNCTUATION. Otherwise
    all punctuation is stripped
    """
    sample_rate=float(sample_rate)

    morse = ''
    for c in to_encode.lower():
        m = None
        if c in MORSE_CODE.keys():
            m = MORSE_CODE[c]
        elif encode_spaces and (c == ' ' or c == '\t'):
            m = MORSE_CODE_SPACE
        elif encode_punctuation and c in MORSE_CODE_PUNCTUATION.keys():
            m = MORSE_CODE_PUNCTUATION[c]

        if m != None:
            morse = morse + m + ' '

            for ditdah in m:
                if ditdah == '.':
                    add_beep(audio, dit, volume=volume, frequency=frequency, sample_rate=sample_rate)
                else:
                    add_beep(audio, dah, volume=volume, frequency=frequency, sample_rate=sample_rate)
                add_silence(audio, sep, sample_rate=sample_rate)
            add_silence(audio, char_sep, sample_rate=sample_rate)
    return morse


if __name__=='__main__':
    parser = argparse.ArgumentParser('Generates a WAV file containing a morse-code encoding of a provided string')
    parser.add_argument('-o,--output', nargs=1, metavar='output.wav', dest='output_path', action='store', type=str, help='The path to the output WAV file', default=['output.wav']  )
    parser.add_argument('-S,--space', dest='encode_spaces', action='store_true', help='Encode spaces in morse; otherwise spaces are stripped')
    parser.add_argument('-P,--punctuation', dest='encode_punctuation', action='store_true', help='Encode punctuation in morse; otherwise punctuation is stripped')
    parser.add_argument('-d,--dit', nargs=1, metavar='DIT_MS', dest='dit_ms', action='store', type=float, help='The duration in ms of a . (ms)', default=[DIT_DURATION_MS])
    parser.add_argument('-D,--dah', nargs=1, metavar='DAH_MS', dest='dah_ms', action='store', type=float, help='The duration in ms of a - (ms)', default=[DAH_DURATION_MS])
    parser.add_argument('-s,--sep', nargs=1, metavar='SEP_MS', dest='sep_ms', action='store', type=float, help='The duration of the silence between consecutive beeps in the same letter (ms)', default=[DITDAH_SEPARATION_MS])
    parser.add_argument('-c,--char-sep', nargs=1, metavar='CHAR_SEP_MS', dest='char_sep_ms', action='store', type=float, help='The duration the silence between characters (ms)', default=[LETTER_SEPARATION_MS])
    parser.add_argument('-v,--volume', nargs=1, metavar='VOL', dest='volume', action='store', type=float, help='The volume of the output file (0-1)', default=[VOLUME])
    parser.add_argument('-f,--freq', nargs=1, metavar='FREQ', dest='frequency', action='store', type=float, help='The frequency of the output tones (Hz)', default=[FREQUENCY_HZ])
    parser.add_argument('-b,--bitrate', nargs=1, metavar='SAMPLE_RATE', dest='sample_rate', action='store', type=float, help='The output bitrate rate', default=[SAMPLE_RATE])
    parser.add_argument('text', metavar='TEXT', action='store', type=str, help='The text to encode')

    args = parser.parse_args()

    params = """Parameters:
      . : {dit}
      - : {dah}
     sep: {sep}
char_sep: {char_sep}
  volume: {vol}
  sample: {sample}
    freq: {freq}
""".format(
        dit = args.dit_ms[0],
        dah = args.dah_ms[0],
        sep = args.sep_ms[0],
        char_sep = args.char_sep_ms[0],
        vol = args.volume[0],
        sample = args.sample_rate[0],
        freq = args.frequency[0])

    print(params)

    if args.dit_ms >= args.dah_ms:
        print("WARNING: . should be shorter than -!\nResulting morse code may be unintelligble!")

    print('Encoding '+args.text)

    audio = []
    morse = encode_string(audio, args.text.lower(),
        encode_spaces=args.encode_spaces, encode_punctuation=args.encode_punctuation,
        dit=args.dit_ms[0], dah=args.dah_ms[0], sep=args.sep_ms[0], char_sep=args.char_sep_ms[0],
        volume=args.volume[0], frequency=args.frequency[0], sample_rate=args.sample_rate[0])
    print(morse)
    print("Saving WAV file to "+args.output_path[0])
    save_wav(args.output_path[0], audio, sample_rate = args.sample_rate[0])
    print("Done")
