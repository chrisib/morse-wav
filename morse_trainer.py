#!/usr/bin/python
"""
A morse-code training program that uses pygame to play audio samples
"""

import morse_wav
import pygame
import random
import os

SAMPLE_RATE = 8000

alphabet = [
    'a','b','c','d','e','f','g',
    'h','i','j','k','l','m','n',
    'o','p','q','r','s','t','u',
    'v','w','x','y','z',
    '0','1','2','3','4',
    '5','6','7','8','9',
]

print("Initializing audio")
n=0
for letter in alphabet:
    n = n+1
    print("{0} of {1}".format(n, len(alphabet)))
    path = "/tmp/{0}.wav".format(letter)
    audio = []
    morse_wav.encode_string(audio, letter, sample_rate=SAMPLE_RATE)
    morse_wav.save_wav(path, audio, sample_rate=SAMPLE_RATE)

pygame.mixer.init(frequency=SAMPLE_RATE)

print("Enter the letter or number you hear")
print("Enter '?' to play the audio again")
print("Enter 'quit' or 'exit' to exit")
print("Stumped? Skip by entering nothing")

correct_guesses = 0;
incorrect_guesses = 0

exit = False
while not exit:
    ch = random.choice(alphabet)
    morse = morse_wav.MORSE_CODE[ch]

    path = "/tmp/{0}.wav".format(ch)
    pygame.mixer.music.load(path)
    pygame.mixer.music.play()

    move_on = False
    re_listens = 0
    while not move_on:
        try:
            answer = raw_input(">>> ").strip().lower()
            if len(answer) == 0:
                print("{0} is {1}".format(morse, ch))
                move_on = True
            elif answer == 'quit' or answer == 'exit':
                move_on = True
                exit = True
            elif answer == '?':
                pygame.mixer.music.play()
                re_listens = re_listens + 1
                if re_listens >= 2:
                    print(morse)
            else:
                if answer == ch:
                    print("CORRECT! {0} is {1}".format(morse,ch))
                    move_on=True
                    correct_guesses = correct_guesses + 1
                else:
                    print("That was not {0}".format(answer))
                    pygame.mixer.music.play()
                    incorrect_guesses = incorrect_guesses + 1
                    re_listens = re_listens + 1
                    if re_listens >= 2:
                        print(morse)
        except:
            move_on = True
            exit = True

print ("Cleaning up...")
for letter in alphabet:
    path = "/tmp/{0}.wav".format(letter)
    os.remove(path)
pygame.mixer.quit()

print("Your final score was {0}/{1}".format(correct_guesses, correct_guesses + incorrect_guesses))
