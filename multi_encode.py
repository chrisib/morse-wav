#!/usr/bin/python
"""
Example of using morse_wav as a support library for encoding an array of strings
"""
import morse_wav

# basic encoding; text & numbers only
to_encode = {
    'correct':'correct',
    'horse':'horse',
    'battery':'battery',
    'staple':'staple',
    'forty-two':'the answer is 42'
}
for k in to_encode.keys():
    s = to_encode[k]
    f = k+'.wav'
    audio = []
    morse = morse_wav.encode_string(audio, s)

    print('{0} --> {1} --> {2}'.format(s, morse, f))

    morse_wav.save_wav(f,audio)

# advanced encoding with punctuation
to_encode = {
    'git-repo-url':'https://gitlab.com/chrisib/morse-wav',
    'google':'https://www.google.com',
    'git-at-gitlab':'git@gitab.com'
}
for k in to_encode.keys():
    s = to_encode[k]
    f = k+'.wav'
    audio = []
    morse = morse_wav.encode_string(audio, s, encode_punctuation=True)

    print('{0} --> {1} --> {2}'.format(s, morse, f))

    morse_wav.save_wav(f,audio)
